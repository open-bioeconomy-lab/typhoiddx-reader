## Background

This project originated from the Open Bioeconomy Lab at the University of Cambridge. We building a multidisciplinary community to deliver its mission and advance research, education and innovation in biotechnology. 

We strive to make our community welcoming and open to everybody, regardless of scholarly or professional background, gender identity and expression, sexual orientation, ability, physical appearance, body size, race, age, economic background, country of origin or employment, religion, and other characteristics. As we come from different backgrounds, it is important to be intentional about providing respectful, equitable spaces --- both online and in person --- for our community to come together and engage in constructive, respectful discourse. 

We particularly strive for equity. We are intentional about actively reducing the inequitable barriers that stand between science and those who create, use, and learn from it.

## Scope of the Code of Conduct

This code of conduct applies to all Open Bioeconomy Lab interactions and spaces, both online and in person. By being part of the Open Bioeconomy Lab team,  attending Open Bioeconomy Lab events and posting in our web channels, you are agreeing to this code of conduct.

## Revision of the Code of Conduct

This Code of Conduct will evolve and be reviewed over time. Any review process will be initiated at the discretion of and organised by the Open Bioeconomy Lab Director. Any issues or changes can be suggested at any time via email or raising an issue in this repository.

## Violations of the Code of Conduct

The Open Bioeconomy Lab takes Code of Conduct violations very seriously. Therefore, individuals who violate this Code may affect their ability to participate in Open Bioeconomy Lab projects, ranging from temporarily being placed into online moderation to, as a last resort, termination of involvement in all projects. 

If you have any questions about our commitment to this framework and/or if you are unsure about any aspects of it, email jcm80@cam.ac.uk and we will provide clarification.

## How It Works

This Code is an effort to maintain a respectful space for everyone and to discuss what might happen if that space is compromised. Please see the guidelines below for community behavior at Biomakespace and our events.

### We listen.

We begin interactions by acknowledging that we are part of a community with complementary goals. When something has happened and someone is uncomfortable, our first choice is to work through it with discussion. We listen to each other.

-   For active listening, we ask questions first, instead of making statements.

-   We give people time and space to respond.

-   We appropriately adjust our behavior when asked to.

-   We know that repeating hurtful behavior after it has been addressed is disrespectful.

-   We avoid this ourselves and help others identify when they are doing it.

### We practice consent.

Note that many forms of harassment do not look like physical or verbal abuse, but still fall into this category. Non-consent can include exhibiting sexual images in public spaces, deliberate intimidation, stalking, following, photography or recording without permission, sustained disruption of talks or conversations, inappropriate physical contact, and unwelcome sexual attention.

## Guidelines for online behavior on chat, Twitter, Trello, website and other Open Bioeconomy Lab forums

Online modes of interaction involve large numbers of people without the helpful presence of visual cues. Because of this, respectful and self-aware online conduct is especially important and difficult. 

In addition to the Code, which remains in play in online spaces, we have specific guidelines for online interactions. If someone violates these guidelines, the Director or manager will place them into moderation by changing that person's posting permission on the relevant list or forum, on the website, or both. Our triple notification standard for moderation means a point person from the Moderators group will:

1) email the person directly with a brief explanation of what was violated, 

2) send a summary email to the rest of the moderators group, 

3) if it happened on a public list (vs a website), notify the list that one of our members has been placed into moderation with a brief explanation of what is not tolerated.

If you wish to begin the process of getting out of moderation, respond to the email sent to you. 


| Do | Don't|
| ---- | ------ |
| Stay on topic to make long threads easier to follow. | Do not send unnecessary one-line responses that effectively "spam" hundreds of people and lower the overall content quality of a conversation. (Exception: expressions of appreciation and encouragement but even then be mindful of people's inboxes and consider sending a personal response!) |
| Start a new thread to help others follow along. Important if your response starts to significantly diverge from the original topic.| Do not respond with off-topic information, making it hard for the large group of readers to follow along. |
| Write short and literal subject lines to help the readers of the list manage the volume of communication | Humor and euphemisms in subject lines are easily misunderstood, although enthusiasm is welcome! |
| Mind your tone. We are not having this conversation in person, so it is all the more important to maintain a tone of respect.| Do not write in an aggressive, disrespectful or mocking tone. Note: writing in all caps is regarded as shouting.|


## How To Report A Problem 

Via email - If you experience or witness something and would prefer to email, email Jenny Molloy via jcm80@cam.ac.uk. 
Reporting should never be done via social media.

## Consequences

Anyone requested to stop behavior that violates the Code of Conduct is expected to comply immediately, even if they disagree with the request.

The Director and delegated members of the Open Bioeconomy Lab may take any action deemed necessary and appropriate, including immediate removal from team or placing into online moderation.

## Acknowledgements

This Code of Conduct drew from other CoCs, including those by Gathering for Open Science Hardware, [Public Lab](https://publiclab.org/notes/Shannon/07-06-2016/public-lab-code-of-conduct), [International Congress of Marine Conservation 2016](http://conbio.org/mini-sites/imcc-2016/registration-participation/code-of-conduct/), and [TransH4CK](http://www.transhack.org/).